function setupCodeFeature() {
   addButton("Réinitialiser", clearDocument);

   addPanel('svgCode',
      [{ fields: [{ label: "Code", type: 'textarea', name: 'code' }] }],
      [
         { label: "Remplacer le document", fct: replaceDocument },
         { label: "Enregistrer sous", fct: downloadCode },
         { label: "Retour", fct: backFromPanel },
      ],
      { label: "Accéder au code", fct: initSvgCodePanel });

   getPanel('svgCode').querySelector('[name="code"]')
      .setAttribute('style', 'width: 90vw; height: 300px; resize: none; font-family: monospace;');

   var saveLink = document.createElement('a');
   saveLink.setAttribute('download', 'schema.svg');
   saveLink.classList.add('hide');
   getPanel('svgCode').appendChild(saveLink);
}


function clearDocument() {
   rebaseDocument(); // Call without any arguments
}


function initSvgCodePanel() {
   getPanel('svgCode').querySelector('[name="code"]').value = new XMLSerializer().serializeToString(svgDocument);
}

function downloadCode() {
   var code = getPanel('svgCode').querySelector('[name="code"]').value;
   var saveLink = getPanel('svgCode').querySelector('a');
   saveLink.setAttribute('href', 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(code));
   saveLink.click();
}

function replaceDocument() {
   var code = getPanel('svgCode').querySelector('[name="code"]').value;
   svgRoot.innerHTML = code;
   var newRoot = svgRoot.firstElementChild;
   svgRoot.setAttribute('width', newRoot.getAttribute('width'));
   svgRoot.setAttribute('height', newRoot.getAttribute('height'));
   svgRoot.innerHTML = newRoot.innerHTML;
   var elemsWithNsDecl = svgRoot.querySelectorAll('*');
   for (var elemI = 0; elemI < elemsWithNsDecl.length; ++elemI) {
      var elem = elemsWithNsDecl[elemI];
      if (elem.getAttribute('xmlns') == svgRoot.getAttribute('xmlns')) {
         elem.removeAttribute('xmlns');
      }
      if (elem.getAttribute('xmlns:diagram') == svgRoot.getAttribute('xmlns:diagram')) {
         elem.removeAttribute('xmlns:diagram');
      }
   }
   initDocument();
}
