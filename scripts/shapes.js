function setupShapesFeature() {
   Object.assign(layingModes, {
         line: {
            fields: [{ x: 'x1', y: 'y1' }, { x: 'x2', y: 'y2' }],
            create: createLine,
         },
         parallelogram: {
            fields: [{ x: 'x1', y: 'y1' }, { x: 'x2', y: 'y2' }, { x: 'sx', y: 'sy', mode: 'diff' },
               { x: 'rx', y: 'ry', mode: 'diff' }],
            create: createParallelogram,
         },
         ellipse: {
            fields: [{ x: 'x1', y: 'y1', xCtrl: 'cx', yCtrl: 'cy' },
               { x: 'x2', y: 'y2', xCtrl: 'rx', yCtrl: 'ry', modeCtrl: 'diff' }],
            create: createEllipse,
         },
         text: {
            fields: [{ x: 'x', y: 'y' }],
            create: createText,
         },
      });

   addPanel('line',
      [
         {
            fields: [
               { label: "X1", type: 'number', name: 'x1' },
               { label: "Y1", type: 'number', name: 'y1' },
               { label: "X2", type: 'number', name: 'x2' },
               { label: "Y2", type: 'number', name: 'y2' },
            ],
         },
      ],
      [
         { label: "Placer la ligne", fct: layElement },
         { label: "Recommencer", fct: clearFields },
         { label: "Retour", fct: backFromLayPanel },
      ],
      { label: "Placer une ligne", fct: resetLaying });

   addPanel('parallelogram',
      [
         {
            fields: [
               { label: "X1", type: 'number', name: 'x1' },
               { label: "Y1", type: 'number', name: 'y1' },
               { label: "X2", type: 'number', name: 'x2' },
               { label: "Y2", type: 'number', name: 'y2' },
            ],
         },
         {
            label: "Inclinaison",
            fields: [
               { label: "X", type: 'number', name: 'sx' },
               { label: "Y", type: 'number', name: 'sy' },
            ],
         },
         {
            label: "Arrondi",
            fields: [
               { label: "X", type: 'number', name: 'rx' },
               { label: "Y", type: 'number', name: 'ry' },
            ],
         },
      ],
      [
         { label: "Placer le parallélogramme", fct: layElement },
         { label: "Recommencer", fct: clearFields },
         { label: "Retour", fct: backFromLayPanel },
      ],
      { label: "Placer un parallélogramme", fct: resetLaying });

   addPanel('ellipse',
      [
         {
            fields: [
               { label: "X1", type: 'number', name: 'x1' },
               { label: "Y1", type: 'number', name: 'y1' },
               { label: "X2", type: 'number', name: 'x2' },
               { label: "Y2", type: 'number', name: 'y2' },
            ],
         },
         {
            label: "Centre",
            fields: [
               { label: "X", type: 'number', name: 'cx' },
               { label: "Y", type: 'number', name: 'cy' },
            ],
         },
         {
            label: "Rayons",
            fields: [
               { label: "X", type: 'number', name: 'rx' },
               { label: "Y", type: 'number', name: 'ry' },
            ],
         },
      ],
      [
         { label: "Placer l’ellipse", fct: layElement },
         { label: "Recommencer", fct: clearFields },
         { label: "Retour", fct: backFromLayPanel },
      ],
      { label: "Placer une ellipse", fct: resetLaying });

   addPanel('text',
      [
         {
            fields: [
               { label: "X", type: 'number', name: 'x' },
               { label: "Y", type: 'number', name: 'y' },
            ],
         },
         {
            fields: [
               { label: "Caractères", type: 'text', name: 'chars', styleSpec: 'longText' },
            ],
         },
      ],
      [
         { label: "Poser le texte", fct: layElement },
         { label: "Recommencer", fct: clearFields },
         { label: "Retour", fct: backFromLayPanel },
      ],
      { label: "Poser un texte", fct: resetLaying });
}


function createLine(data) {
   var line = null;
   if (hasAllFields(data, ['x1', 'y1', 'x2', 'y2']) && (data.x1 != data.x2 || data.y1 != data.y2)) {
      line = createSvgElement('line', {
            'x1': gridXToDocX(data.x1),
            'y1': gridYToDocY(data.y1),
            'x2': gridXToDocX(data.x2),
            'y2': gridYToDocY(data.y2),
         });
   }
   return line;
}

function createParallelogram(data) {
   var paral = null;
   if (hasAllFields(data, ['x1', 'y1', 'x2', 'y2']) && (data.x1 != data.x2 || data.y1 != data.y2)) {
      var x1 = gridXToDocX(data.x1), y1 = gridYToDocY(data.y1), x2 = gridXToDocX(data.x2), y2 = gridYToDocY(data.y2);
      var matrixValues = [1, 0, 0, 1, 0, 0], isSkewed = false;
      if ('sx' in data && data.sx != 0) {
         var sx = gridWToDocW(data.sx);
         matrixValues[2] = sx / (y2 - y1);
         matrixValues[4] = -y1 * matrixValues[2];
         isSkewed = true;
      }
      if ('sy' in data && data.sy != 0) {
         var sy = gridHToDocH(data.sy);
         matrixValues[1] = sy / (x2 - x1);
         matrixValues[5] = -x1 * matrixValues[1];
         isSkewed = true;
      }
      paral = createSvgElement('rect', {
            'x': Math.min(x1, x2),
            'y': Math.min(y1, y2),
            'width': Math.abs(x2 - x1),
            'height': Math.abs(y2 - y1),
         });
      if (isSkewed) {
         paral.setAttribute('transform', 'matrix(' + matrixValues.join(',') + ')');
      }
      if ('rx' in data && data.rx != 0) {
         paral.setAttribute('rx', Math.abs(gridWToDocW(data.rx)));
      }
      if ('ry' in data && data.ry != 0 && data.ry != data.rx) {
         paral.setAttribute('ry', Math.abs(gridHToDocH(data.ry)));
      }
   }
   return paral;
}

function createEllipse(data) {
   if ('rx' in data && !('ry' in data)) data.ry = data.rx;
   ['x', 'y'].forEach(function(coordName) {
      if (data[coordName + '1'] && data[coordName + '2']) {
         data['c' + coordName] = (data[coordName + '1'] + data[coordName + '2']) / 2;
         data['r' + coordName] = (data[coordName + '2'] - data[coordName + '1']) / 2;
      } else if (data[coordName + '1'] && data['c' + coordName]) {
         data['r' + coordName] = data['c' + coordName] - data[coordName + '1'];
      } else if (data[coordName + '2'] && data['c' + coordName]) {
         data['r' + coordName] = data[coordName + '2'] - data['c' + coordName];
      } else if (data[coordName + '1'] && data['r' + coordName]) {
         data['c' + coordName] = data[coordName + '1'] + data['r' + coordName];
      } else if (data[coordName + '2'] && data['r' + coordName]) {
         data['c' + coordName] = data[coordName + '2'] - data['r' + coordName];
      }
   });
   var ellipse = null;
   if (hasAllFields(data, ['cx', 'cy', 'rx', 'ry']) && (data.rx != 0 && data.ry != 0)) {
      if (Math.abs(data.rx) == Math.abs(data.ry)) {
         ellipse = createSvgElement('circle', {
               'cx': gridXToDocX(data.cx),
               'cy': gridYToDocY(data.cy),
               'r': Math.abs(gridWToDocW(data.rx)),
            });
      } else {
         ellipse = createSvgElement('ellipse', {
               'cx': gridXToDocX(data.cx),
               'cy': gridYToDocY(data.cy),
               'rx': Math.abs(gridWToDocW(data.rx)),
               'ry': Math.abs(gridHToDocH(data.ry)),
            });
      }
   }
   return ellipse;
}

function createText(data) {
   var text = null;
   if (hasAllFields(data, ['x', 'y', 'chars'])) {
      text = createSvgElement('text', {
            'x': gridXToDocX(data.x),
            'y': gridYToDocY(data.y),
            'dy': '.5ex',
         },
         data.chars);
   }
   return text;
}


function adjustElement(elem) {
   if (elem.tagName == 'line') {
      var style = svgContainer.contentWindow.getComputedStyle(elem);
      var attrsNames = ['x1', 'y1', 'x2', 'y2'];
      var attrs = {};
      for (var attrI = 0; attrI < attrsNames.length; ++attrI) {
         var attrValue = elem.getAttribute('diagram:' + attrsNames[attrI]) || elem.getAttribute(attrsNames[attrI]);
         elem.removeAttribute('diagram:' + attrsNames[attrI]);
         elem.setAttribute(attrsNames[attrI], attrValue);
         attrs[attrsNames[attrI]] = parseFloat(attrValue);
      }
      var angle = Math.atan2(attrs.y2 - attrs.y1, attrs.x2 - attrs.x1);
      var length = Math.sqrt(Math.pow(attrs.x2 - attrs.x1, 2) + Math.pow(attrs.y2 - attrs.y1, 2));
      var markerData = { 'marker-start': 1, 'marker-end': -1 }, pointI = 0;
      for (var markerName in markerData) {
         ++pointI;
         if (style[markerName] != 'none') {
            var marker = svgElement(style[markerName].match(/"#(.+)"/)[1]);
            var requiredLength = parseFloat(style['stroke-width']) * marker.markerWidth.baseVal.value *
               (1 - (marker.refX.baseVal.value - marker.viewBox.baseVal.x) / marker.viewBox.baseVal.width);
            if (length > requiredLength) {
               length -= requiredLength;
               [['x', Math.cos], ['y', Math.sin]].forEach(function(coordData) {
                  elem.setAttributeNS(svgRoot.getAttribute('xmlns:diagram'), 'diagram:' + coordData[0] + pointI,
                     elem.getAttribute(coordData[0] + pointI));
                  elem.setAttribute(coordData[0] + pointI,
                     attrs[coordData[0] + pointI] + coordData[1](angle) * requiredLength * markerData[markerName]);
               });
            } else {
               elem.style[markerName] = 'none';
            }
         }
      }
   }
}
