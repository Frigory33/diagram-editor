var svgDocument, diagramNS;
var slideIndex, slideStepIndex;


window.addEventListener('load', function() {
   var buttons = gui.querySelectorAll('#loadSlideShowPanel button');
   buttons[0].onclick = function() {
      var slideShowFileName = 'diagrams/' + gui.element('slideShowFileName').value;
      gui.querySelector('object').setAttribute('data', slideShowFileName);
      gui.element('panels').classList.add('hide');
   };
   buttons[1].onclick = function() {
      gui.querySelector('object').contentWindow.location.reload();
      var slideShowFileName = 'diagrams/' + gui.element('slideShowFileName').value;
      gui.querySelector('object').setAttribute('data', slideShowFileName);
   };


   addKeyboardFct('Escape', function() {
      gui.element('panels').classList.toggle('hide');
      updateDocumentWrapperLayout();
   });
   addKeyboardFct('ArrowLeft', function() {
      showSlideStep(slideStepIndex - 1) || showSlide(slideIndex - 1);
   });
   addKeyboardFct('ArrowRight', function() {
      showSlideStep(slideStepIndex + 1) || showSlide(slideIndex + 1);
   });
   addKeyboardFct('PageUp', function() {
      showSlide(slideIndex - 1);
   });
   addKeyboardFct('PageDown', function() {
      showSlide(slideIndex + 1);
   });
   addKeyboardFct('Home', function() {
      showSlide(1);
   });

   gui.querySelector('object').onload = function() {
      svgDocument = gui.querySelector('object').contentDocument;
      diagramNS = svgDocument.rootElement.getAttribute('xmlns:diagram');
      svgDocument.onkeydown = svgDocument.onmouseup = gui.onkeydown;
      svgDocument.onkeyup = gui.onkeyup;
      gui.querySelector('title').textContent = svgDocument.querySelector('title').textContent;
      updateDocumentWrapperLayout();
      showSlide(1);
   };
});


function showSlide(index) {
   var slide = selectByXPath(svgDocument, './/*[@diagram:slideIndex="' + index + '"]');
   if (!slide) {
      return false;
   }
   var previousSlide = selectByXPath(svgDocument, './/*[@diagram:slideIndex][@diagram:slideShowCurrent]');
   if (previousSlide) {
      previousSlide.removeAttribute('diagram:slideShowCurrent');
   }
   slideIndex = index;
   slideStepIndex = 0;
   slide.setAttributeNS(diagramNS, 'diagram:slideShowCurrent', '');
   showSlideStep(1);
   return true;
}

function showSlideStep(index) {
   var slide = selectByXPath(svgDocument, './/*[@diagram:slideIndex][@diagram:slideShowCurrent]');
   var slideStepsGroups = selectAllByXPath(slide, './/*[contains(@diagram:slideStepsIndexes, " ' + index + ' ")]');
   if (slideStepsGroups.length == 0) {
      return false;
   }
   selectAllByXPath(slide, './/*[@diagram:slideShowCurrent]').forEach(function(stepGroup) {
      stepGroup.removeAttribute('diagram:slideShowCurrent');
   });
   slideStepIndex = index;
   slideStepsGroups.forEach(function(stepGroup) {
      stepGroup.setAttributeNS(diagramNS, 'diagram:slideShowCurrent', '');
   });
   return true;
}
