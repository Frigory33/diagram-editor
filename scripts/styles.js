var currentStyleId = [];

var styleProperties = [
   colorProperty('fillColor', 'fill'),
   numberProperty('strokeWidth', 'stroke-width', 'px'),
   colorProperty('strokeColor', 'stroke'),
   markerProperty('firstVertex', 'marker-start'),
   markerProperty('intermediateVertexes', 'marker-mid'),
   markerProperty('lastVertex', 'marker-end'),
   numberProperty('fontSize', 'font-size', 'px'),
   booleanProperty('fontIsBold', 'font-weight', 'bold'),
   booleanProperty('fontIsItalic', 'font-style', 'italic'),
   stringProperty('fontLine', 'text-decoration'),
];

function numberProperty(name, cssName, unit) {
   return {
      cssName: cssName,
      getCssValue: function(data) {
         return !(name in data) ? null :
            data[name] + (unit == undefined ? '' : unit);
      },
      type: function(data, style) {
         data[name] = parseFloat(style[cssName]);
      },
   };
}
function stringProperty(name, cssName) {
   return {
      cssName: cssName,
      getCssValue: function(data) {
         return !(name in data) ? null : data[name];
      },
      type: function(data, style) {
         data[name] = style[cssName];
      },
   };
}
function colorProperty(name, cssName) {
   return {
      cssName: cssName,
      getCssValue: function(data) {
         return !(name in data) ? null : data[name];
      },
      type: function(data, style) {
         data[name] = colorFromRgbToHex(style[cssName]);
      },
   };
}
function booleanProperty(name, cssName, cssValue) {
   return {
      cssName: cssName,
      getCssValue: function(data) {
         return !(name in data) ? null : cssValue;
      },
      type: function(data, style) {
         data[name] = style[cssName] == cssValue;
      },
   };
}
function markerProperty(name, cssName) {
   var prop = {
      cssName: cssName,
      getCssValue: function(data) {
         return !(name in data) ? null : 'url("#' + prop.getMarkerId(data) + '")';
      },
      type: function(data, style) {
         var values = style[cssName].match(/^url\("#vertex_(\w+?)_\w+?(_(\w+?))?"\)$/);
         data[name] = values[1];
         if (values[3] != undefined) {
            data[name + 'Size'] = parseFloat(values[3].replace('comma', '.'));
         }
      },
      prepare: function(data) {
         var markerId = prop.getMarkerId(data);
         var marker = svgElement(markerId);
         if (marker == null) {
            var originalMarker = svgElement('vertex_' + data[name]);
            var marker = originalMarker.cloneNode(true);
            marker.id = markerId;
            marker.style['fill'] = data.strokeColor || '#000000';
            if (name + 'Size' in data) {
               var quotient = data[name + 'Size'] / parseFloat(originalMarker.getAttribute('markerHeight'));
               marker.setAttribute('markerWidth', parseFloat(originalMarker.getAttribute('markerWidth')) * quotient);
               marker.setAttribute('markerHeight', data[name + 'Size']);
            }
            svgElement('customDefs').appendChild(marker);
         }
      },
      unprepare: function(data) {
         var marker = svgElement(prop.getMarkerId(data));
         if (marker != null) {
            marker.remove();
         }
      },
      getMarkerId: function(data) {
         var colorSuffix = '_' + (data.strokeColor || '#000000ff').substr(1);
         var sizeSuffix = name + 'Size' in data ? '_' + data[name + 'Size'].toString().replace('.', 'comma') : '';
         return !(name in data) ? null : 'vertex_' + data[name] + colorSuffix + sizeSuffix;
      },
   };
   return prop;
}

var stylesList = {
   lists: [],
   add: function(styleId) {
      stylesList.lists.forEach(function(list) {
         var listItem = gui.createElement('option');
         listItem.setAttribute('data-styleId', styleId);
         listItem.text = styleId;
         listItem.value = styleId;
         list.add(listItem);
      });
   },
   getUp: function(styleId) {
      stylesList.lists.forEach(function(list) {
         var listItem = list.querySelector('[data-styleId="' + styleId + '"]');
         if (listItem && listItem.index != 0 && (listItem.index != 1 || list.firstElementChild.value != '')) {
            list.add(listItem, listItem.index - 1);
         }
      });
   },
   remove: function(styleId) {
      stylesList.lists.forEach(function(list) {
         list.querySelector('[data-styleId="' + styleId + '"]').remove();
      });
   },
   clear: function() {
      stylesList.lists.forEach(function(list) {
         if (list.firstElementChild && list.firstElementChild.value == '') {
            var listItem_None = list.firstElementChild;
            list.innerHTML = '';
            list.add(listItem_None);
         } else {
            list.innerHTML = '';
         }
      });
   },
};


function setupStylesFeature() {
   var vertexOptions = [
      { label: "—", id: '' },
      { label: "Rond", id: 'disc' },
      { label: "Pointe", id: 'triangle' },
      { label: "Flèche fine", id: 'thinArrow' },
      { label: "Flèche concave", id: 'niceArrow' },
   ];
   addPanel('handleStyles',
      [
         {
            fields: [
               { label: "Liste", type: 'select', name: 'list', options: [{ label: "< Aucune >", id: '' }] },
            ],
         },
         {
            fields: [
               { label: "ID", type: 'text', name: 'id' },
            ],
         },
         {
            label: "Corps",
            fields: [
               { label: "Couleur", type: 'color', name: 'fillColor' },
            ],
         },
         {
            label: "Ligne",
            fields: [
               { label: "Épaisseur", type: 'number', name: 'strokeWidth' },
               { label: "Couleur", type: 'color', name: 'strokeColor' },
            ],
         },
         {
            label: "Sommets",
            fields: [
               { label: "Premier", type: 'select', name: 'firstVertex', options: vertexOptions },
               { label: "Taille (×)", type: 'number', name: 'firstVertexSize' },
               { label: "Dernier", type: 'select', name: 'lastVertex', options: vertexOptions },
               { label: "Taille (×)", type: 'number', name: 'lastVertexSize' },
               { type: 'br' },
               { label: "Intermédiaires", type: 'select', name: 'intermediateVertexes', options: vertexOptions },
               { label: "Taille (×)", type: 'number', name: 'intermediateVertexesSize' },
            ],
         },
         {
            label: "Texte",
            fields: [
               { label: "Taille", type: 'number', name: 'fontSize' },
               { label: "Gras", type: 'checkbox', name: 'fontIsBold' },
               { label: "Italique", type: 'checkbox', name: 'fontIsItalic' },
               { label: "Ligne", type: 'select', name: 'fontLine', options: [
                     { label: "—", id: '' },
                     { label: "Surlignement", id: 'overline' },
                     { label: "Barrement", id: 'line-through' },
                     { label: "Soulignement", id: 'underline' },
                  ] },
            ],
         },
      ],
      [
         { label: "Enregistrer le style", fct: saveStyle },
         { label: "Monter le style", fct: getStyleUp },
         { label: "Supprimer le style", fct: deleteStyle },
         { label: "Retour", fct: backFromPanel },
      ],
      { label: "Modifier les styles" });
   var handleStylesList = getPanel('handleStyles').querySelector('[name="list"]');
   stylesList.lists.push(handleStylesList);
   handleStylesList.onchange = typeStyleValues;

   addPanel('useStyles',
      [
         {
            fields: [
               { label: "Styles sélectionnés", type: 'multiselect', name: 'selectedStyles' },
            ],
         },
      ],
      [
         { label: "Valider", fct: backFromPanel },
         { label: "Affecter à la sélection", fct: assignStylesToSelection },
         { label: "Sélectionner les éléments correspondants", fct: selectStyledElems },
      ],
      { label: "Utiliser les styles" });
   var useStylesList = getPanel('useStyles').querySelector('[name="selectedStyles"]');
   stylesList.lists.push(useStylesList);
   useStylesList.onchange = selectStyles;

   docInitFcts.push(catchStyles);
}


function catchStyles() {
   stylesList.clear();
   var rules = svgElement('customCSS').sheet.cssRules;
   for (var ruleI = 0; ruleI < rules.length; ++ruleI) {
      var styleId = rules[ruleI].selectorText.substr(1);
      stylesList.add(styleId);
   }
}

function saveStyle() {
   var data = getPanelValues();

   var styleId = (data.id || '').match(/\w/g);
   if (styleId != null) {
      styleId = styleId.join('');
      var ruleIndex = svgElement('customCSS').sheet.cssRules.length;

      var handleStylesList = getPanel().querySelector('[name="list"]');
      var styleOptionInList = handleStylesList.querySelector('[data-styleId="' + styleId + '"]');
      if (styleOptionInList != null) {
         handleStylesList.selectedIndex = styleOptionInList.index;
         ruleIndex = handleStylesList.selectedIndex - 1;
         unprepareStyle(ruleIndex);
      }

      var ruleCode = '.' + styleId + ' {\n';
      for (var propI = 0; propI < styleProperties.length; ++propI) {
         var prop = styleProperties[propI];
         var value = prop.getCssValue(data);
         if (value != null) {
            if (prop.prepare) {
               prop.prepare(data);
            }
            ruleCode += '\t' + prop.cssName + ': ' + value + ';\n';
         }
      }
      ruleCode += '}\n';
      svgElement('customCSS').sheet.insertRule(ruleCode, ruleIndex);
      updateCustomCss();

      var affectedElems = svgElement('content').getElementsByClassName(styleId);
      for (var elemI = 0; elemI < affectedElems.length; ++elemI) {
         adjustElement(affectedElems[elemI]);
      }

      if (styleOptionInList == null) {
         stylesList.add(styleId);
         handleStylesList.selectedIndex = handleStylesList.options.length - 1;
      }
   }
}

function getStyleUp() {
   var handleStylesList = getPanel().querySelector('[name="list"]');
   var ruleIndex = handleStylesList.selectedIndex - 1;
   if (ruleIndex > 0) {
      var ruleCode = svgElement('customCSS').sheet.cssRules[ruleIndex].cssText;
      svgElement('customCSS').sheet.deleteRule(ruleIndex);
      svgElement('customCSS').sheet.insertRule(ruleCode, ruleIndex - 1);
      updateCustomCss();
      stylesList.getUp(handleStylesList.value);
   }
}

function deleteStyle() {
   var handleStylesList = getPanel().querySelector('[name="list"]');
   if (handleStylesList.value == '') {
      clearStyleValues();
   } else {
      unprepareStyle(handleStylesList.selectedIndex - 1);
      updateCustomCss();
      stylesList.remove(handleStylesList.value);
      handleStylesList.selectedIndex = 0;
   }
}

function unprepareStyle(ruleIndex) {
   var style = svgElement('customCSS').sheet.cssRules[ruleIndex].style;
   var data = {};
   for (var propI = 0; propI < styleProperties.length; ++propI) {
      var prop = styleProperties[propI];
      if (style[prop.cssName]) {
         prop.type(data, style);
      }
   }
   for (var propI = 0; propI < styleProperties.length; ++propI) {
      var prop = styleProperties[propI];
      if (prop.unprepare && style[prop.cssName]) {
         prop.unprepare(data);
      }
   }
   svgElement('customCSS').sheet.deleteRule(ruleIndex);
}

function updateCustomCss() {
   var customCss = '\n';
   var rules = svgElement('customCSS').sheet.cssRules;
   for (var ruleI = 0; ruleI < rules.length; ++ruleI) {
      customCss += rules[ruleI].cssText + '\n';
   }
   svgElement('customCSS').innerHTML = customCss;
}


function clearStyleValues() {
   var data = {};
   var fields = getPanel().querySelectorAll('[name]');
   for (var fieldI = 1; fieldI < fields.length; ++fieldI) {
      data[fields[fieldI].getAttribute('name')] = '';
   }
   setPanelValues(getPanel(), data);
}

function typeStyleValues() {
   var handleStylesList = getPanel().querySelector('[name="list"]');
   clearStyleValues();
   if (handleStylesList.value != '') {
      var data = { id: handleStylesList.value };
      var style = svgElement('customCSS').sheet.cssRules[handleStylesList.selectedIndex - 1].style;
      for (var propI = 0; propI < styleProperties.length; ++propI) {
         var prop = styleProperties[propI];
         if (style[prop.cssName]) {
            prop.type(data, style);
         }
      }
      setPanelValues(getPanel(), data);
   }
}


function selectStyles() {
   var styleItems = getPanel().querySelector('[name="selectedStyles"]').options;
   currentStyleId = [];
   for (var styleI = 0; styleI < styleItems.length; ++styleI) {
      var styleItem = styleItems[styleI];
      if (styleItem.selected) {
         currentStyleId.push(styleItem.value);
      }
   }
}

function assignStylesToSelection() {
   svgElement('content').querySelectorAll('.selected').forEach(function(elem) {
      elem.removeAttribute('class');
      currentStyleId.forEach(function(styleId) {
         elem.classList.add(styleId);
      });
      elem.classList.add('selected');
   });
}

function selectStyledElems() {
   selectElement(null);
   svgElement('content').querySelectorAll('*').forEach(function(elem) {
      if (elem.classList.length == currentStyleId.length) {
         var containsAll = true;
         for (var classI = 0; classI < currentStyleId.length; ++classI) {
            containsAll &&= elem.classList.contains(currentStyleId[classI]);
         }
         if (containsAll) {
            selectElement(elem, true);
         }
      }
   });
}


function colorFromRgbToHex(colorStr) {
   var value = '#' + colorStr.match(/[\d\.]+/g).map(to2HexDigits).join('');
   if (value.length == 7) value += 'ff';
   return value;
}
