var grid = null;


function setupGridFeature() {
   addPanel('grid',
      [
         {
            fields: [
               { label: "X1", type: 'number', name: 'x1', defVal: 1 },
               { label: "X2", type: 'number', name: 'x2', defVal: 10 },
               { label: "Y1", type: 'number', name: 'y1', placeholder: 'X1' },
               { label: "Y2", type: 'number', name: 'y2', placeholder: 'X2' },
            ],
         },
         {
            label: "Décalage",
            fields: [
               { label: "X (px)", type: 'number', name: 'docX0', placeholder: 'bL÷2' },
               { label: "Y (px)", type: 'number', name: 'docY0', placeholder: 'X ou bH÷2' },
            ],
         },
         {
            label: "Marge",
            fields: [
               { label: "X (px)", type: 'number', name: 'mx', defVal: 0 },
               { label: "Y (px)", type: 'number', name: 'my', placeholder: 'X' },
            ],
         },
         {
            label: "Bloc",
            fields: [
               { label: "Largeur (px)", type: 'number', name: 'bw', placeholder: 'docL÷L' },
               { label: "Hauteur (px)", type: 'number', name: 'bh', placeholder: 'bL ou docH÷H' },
            ],
         },
      ],
      [
         { label: "Disposer la grille", fct: setupGrid },
         { label: "Comprendre la grille", fct: typeGridValues },
         { label: "Supprimer la grille", fct: removeGrid },
         { label: "Annuler", fct: backFromPanel },
      ],
      { label: "Travailler la grille" });

   docInitFcts.push(catchGrid);
}


function gridXToDocX(gridX) {
   return grid == null ? gridX : grid.docX0 + (gridX - grid.x0) * grid.bw;
}
function gridYToDocY(gridY) {
   return grid == null ? svgRoot.height.baseVal.value - 1 - gridY : grid.docY0 - (gridY - grid.y0) * grid.bh;
}
function gridWToDocW(gridW) {
   return grid == null ? gridW : gridW * grid.bw;
}
function gridHToDocH(gridH) {
   return grid == null ? -gridH : -gridH * grid.bh;
}

function docXToGridX(docX) {
   return grid == null ? docX : Math.max(grid.x1, Math.min(Math.round((docX - grid.docX0) / grid.bw + grid.x0), grid.x2));
}
function docYToGridY(docY) {
   return grid == null ? svgRoot.height.baseVal.value - 1 - docY :
      Math.max(grid.y1, Math.min(Math.round(-(docY - grid.docY0) / grid.bh + grid.y0), grid.y2));
}


function catchGrid() {
   grid = null;
   for (var attrI = 0; attrI < svgElement('grid').attributes.length; ++attrI) {
      var attr = svgElement('grid').attributes[attrI];
      if (attr.prefix == 'diagram') {
         if (grid == null) {
            grid = {};
         }
         grid[attr.localName] = parseFloat(attr.value);
      }
   }
}

function setupGrid() {
   var panel = getPanel();
   var data = getPanelValues(panel);

   grid = {};

   Object.assign(grid, data);
   var couples = [['x1', 'y1'], ['x2', 'y2'], ['x0', 'y0'], ['docX0', 'docY0'], ['mx', 'my'], ['bw', 'bh']];
   for (var coupleI = 0; coupleI < couples.length; ++coupleI) {
      var first = couples[coupleI][0], second = couples[coupleI][1];
      if (!(first in grid) && !(second in grid)) {
      } else if (!(second in grid)) {
         grid[second] = grid[first];
      } else if (!(first in grid)) {
         grid[first] = grid[second];
      }
   }

   if (!('x0' in grid && 'y0' in grid)) {
      grid.x0 = grid.x1;
      grid.y0 = grid.y1;
   }
   if (!('mx' in grid && 'my' in grid)) {
      grid.mx = grid.my = 0;
   }

   grid.w = -Math.min(grid.x0, grid.x1) + Math.max(grid.x0, grid.x2) + 1;
   grid.h = -Math.min(grid.y0, grid.y1) + Math.max(grid.y0, grid.y2) + 1;

   if ('bw' in grid && 'bh' in grid) {
      grid.docW = grid.w * grid.bw + grid.mx * 2;
      grid.docH = grid.h * grid.bh + grid.my * 2;
      svgRoot.setAttribute('width', grid.docW);
      svgRoot.setAttribute('height', grid.docH);
   } else {
      grid.docW = parseInt(svgRoot.getAttribute('width'));
      grid.docH = parseInt(svgRoot.getAttribute('height'));
      grid.bw = (grid.docW - grid.mx * 2) / grid.w;
      grid.bh = (grid.docH - grid.my * 2) / grid.h;
   }

   if (!('docX0' in grid && 'docY0' in grid)) {
      grid.docX0 = grid.mx + grid.bw / 2 + (grid.x0 > grid.x1 ? grid.x0 - grid.x1 : 0) * grid.bw;
      grid.docY0 = grid.docH - grid.my - grid.bh / 2 - (grid.y0 > grid.y1 ? grid.y0 - grid.y1 : 0) * grid.bh;
   } else {
      grid.docX0 = grid.mx;
      grid.docY0 = grid.docH - grid.my - grid.docY0;
   }

   drawGrid();

   if (panel.localId == 'grid') {
      backFromPanel();
   }
}


function drawGrid() {
   svgElement('grid').innerHTML = '';

   for (var gridProp in grid) {
      svgElement('grid').setAttributeNS(svgRoot.getAttribute('xmlns:diagram'), 'diagram:' + gridProp, grid[gridProp]);
   }

   for (var x = grid.x1; x <= grid.x2; ++x) {
      var line = createSvgElement('line', {
            'x1': gridXToDocX(x),
            'x2': gridXToDocX(x),
            'y1': 0,
            'y2': grid.docH,
         });
      svgElement('grid').appendChild(line);
   }

   for (var y = grid.y1; y <= grid.y2; ++y) {
      var line = createSvgElement('line', {
            'x1': 0,
            'x2': grid.docW,
            'y1': gridYToDocY(y),
            'y2': gridYToDocY(y),
         });
      svgElement('grid').appendChild(line);
   }
}

function removeGrid() {
   grid = null;

   for (var attrI = 0; attrI < svgElement('grid').attributes.length; ++attrI) {
      var attr = svgElement('grid').attributes[attrI];
      if (attr.prefix == 'diagram') {
         svgElement('grid').removeAttributeNode(attr);
         --attrI;
      }
   }

   svgElement('grid').innerHTML = '';

   backFromPanel();
}


function typeGridValues() {
   var gridValues = Object.assign({}, grid);
   gridValues.docY0 = gridValues.docH - gridValues.docY0;

   var fields = gui.querySelectorAll('#gridPanel [name]');
   for (var fieldI = 0; fieldI < fields.length; ++fieldI) {
      var field = fields[fieldI];
      field.value = gridValues[field.getAttribute('name')];
   }
}
