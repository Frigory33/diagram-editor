var layingModes = {};

var pointsList, pointIndex;


function setupLayingFeature() {
   addKeyboardFct('', function() {
      if (getPanel().localId in layingModes) {
         highlightCurrentFields();
         showShadow();
      }
   });

   addKeyboardFct('Mouse left', function(evt) {
      if (getPanel().localId in layingModes) {
         addLayingPoint({ x: docXToGridX(evt.clientX), y: docYToGridY(evt.clientY) }, '');
      } else {
         selectElement(svgDocument.elementFromPoint(evt.clientX, evt.clientY));
      }
   });
   addKeyboardFct('Ctrl-Mouse left', function(evt) {
      if (getPanel().localId in layingModes) {
         addLayingPoint({ x: docXToGridX(evt.clientX), y: docYToGridY(evt.clientY) }, 'Ctrl');
      }
   });
   addKeyboardFct('Shift-Mouse left', function(evt) {
      selectElement(svgDocument.elementFromPoint(evt.clientX, evt.clientY), true);
   });
   addKeyboardFct('Mouse right', function() {
      if (getPanel().localId in layingModes) {
         removeLayingPoint();
      }
   });
   addKeyboardFct('Ctrl-Mouse right', function() {
      if (getPanel().localId in layingModes) {
         removeLayingPoint('Ctrl');
      }
   });

   addKeyboardFct('F8', function() {
      if (getPanel().localId in layingModes) {
         if (pointIndex > 0) {
            --pointIndex;
            highlightCurrentFields();
            showShadow();
         }
      }
   });
   addKeyboardFct('F9', function() {
      if (getPanel().localId in layingModes) {
         if (pointIndex < layingModes[getPanel().localId].fields.length) {
            ++pointIndex;
            highlightCurrentFields();
            showShadow();
         }
      }
   });
   addKeyboardFct('Delete', function() {
      svgElement('content').querySelectorAll('.selected').forEach(function(elem) {
         var elemParent = elem.parentElement;
         elem.remove();
         if (elemParent.childElementCount == 0 && elemParent.getAttribute('diagram:slideStepsIndexes') != null) {
            elemParent.remove();
         }
      });
   });
   addKeyboardFct('Home', function() {
      svgElement('content').querySelectorAll('.selected').forEach(function(elem) {
         putElementOnTop(elem, elem.parentElement);
      });
   });
   addKeyboardFct('Ctrl-A', function() {
      svgElement('content').querySelectorAll(':not(g)').forEach(function(elem) {
         elem.classList.add('selected');
      });
   });
   addKeyboardFct('Escape', function() {
      selectElement();
   });
}


function addLayingPoint(point, suffix) {
   var fields = getPointFields(suffix);
   if (fields && !(fields.mode == 'diff' && pointsList.length <= 0)) {
      ['x', 'y'].forEach(function(coordName) {
         if (fields[coordName]) {
            fields[coordName].value = fields.mode == 'diff' ? point[coordName] - pointsList[pointsList.length - 1][coordName] :
               point[coordName];
         }
      });
      pointsList.push(point);
      ++pointIndex;
      highlightCurrentFields();
   }
   getPanel().querySelector('button:first-of-type').focus();
}

function removeLayingPoint(suffix) {
   if (pointIndex > 0) {
      if (pointsList.length > 0) {
         pointsList.pop();
      }
      --pointIndex;
      var fields = getPointFields(suffix);
      ['x', 'y'].forEach(function(coordName) {
         if (fields[coordName]) {
            fields[coordName].value = '';
         }
      });
      highlightCurrentFields();
      showShadow();
   }
}


function selectElement(element, additiveMode) {
   if (!additiveMode) {
      svgElement('content').querySelectorAll('.selected').forEach(function(elem) {
         elem.classList.remove('selected');
      });
   }
   if (element && element.closest('#content')) {
      element.classList.toggle('selected');
   }
}


function resetLaying() {
   pointsList = [];
   pointIndex = 0;
   highlightCurrentFields();
   showShadow();
}

function layElement() {
   var elemParent = svgElement('content');
   if (currentSlide) {
      elemParent = currentSlide;
      if (currentSteps != null) {
         elemParent = getStepsContainer(currentSteps);
      }
   }
   layElementSomewhere(elemParent, getPanelValues());
   if (currentKeyModifiers.indexOf('Shift') >= 0) {
      clearFields();
   } else {
      resetLaying();
   }
   getPanel().querySelector('button:nth-of-type(2)').focus();
}

function layElementSomewhere(container, data) {
   var elem = layingModes[getPanel().localId].create(data);
   if (elem != null) {
      currentStyleId.forEach(function(styleId) {
         elem.classList.add(styleId);
      });
      putElementOnTop(elem, container);
      adjustElement(elem);
   }
}

function putElementOnTop(elem, container) {
   var firstGroup = container.querySelector('g');
   if (firstGroup != null) {
      firstGroup.before(elem);
   } else {
      container.appendChild(elem);
   }
}


function getPointFields(suffix) {
   var fieldNames = layingModes[getPanel().localId].fields;
   if (pointIndex >= fieldNames.length) {
      return null;
   }
   if (suffix == undefined) suffix = currentKeyModifiers.join('');
   fieldNames = fieldNames[pointIndex];
   var fields = {}, fieldFound = false;
   ['x', 'y'].forEach(function(coordName) {
      if (fieldNames[coordName + suffix]) {
         var field = getPanel().querySelector('[name="' + fieldNames[coordName + suffix] + '"]');
         if (field != null) {
            fields[coordName] = field;
            fieldFound = true;
         }
      }
   });
   if (!fieldFound) {
      return null;
   }
   fields.mode = fieldNames['mode' + suffix];
   return fields;
}

function highlightCurrentFields() {
   getPanel().querySelectorAll('.currentField').forEach(function(fieldContainer) {
      fieldContainer.classList.remove('currentField');
   });
   var fields = getPointFields();
   if (fields) {
      ['x', 'y'].forEach(function(coordName) {
         if (fields[coordName]) {
            fields[coordName].parentElement.classList.add('currentField');
         }
      });
   }
}

function clearFields() {
   getPanel().querySelectorAll('[name]').forEach(function(field) {
      field.value = '';
   });
   resetLaying();
}


function backFromLayPanel() {
   resetLaying();
   backFromPanel();
   showShadow();
}
