var currentSlide = null, currentSteps = null;


function setupSlidesFeature() {
   function notImplementedYet() {
      alert("Pas encore développé.");
   }

   addPanel('slides',
      [
         {
            fields: [
               { label: "Diapositive", type: 'select', name: 'selectedSlide', options: [{ label: "< Aucune >", id: '' }] },
            ],
         },
         {
            fields: [
               { label: "Étapes", type: 'multiselect', name: 'selectedSteps' },
            ],
         },
      ],
      [
         { label: "Ajouter une diapositive", fct: addSlide },
         { label: "Monter la diapositive", fct: putSlideUp },
         { label: "Supprimer la diapositive", fct: removeSlide },
         { label: "Ajouter une étape", fct: addSlideStep },
         { label: "Supprimer la dernière étape", fct: removeLastSlideStep },
         { label: "Sortir la sélection", fct: putSelectionOutOfSlides },
         { label: "Insérer la sélection", fct: putSelectionInCurrentSlideSteps },
         { label: "Retour", fct: backFromPanel },
      ],
      { label: "Organiser les diapositives" });

   var slidesList = getPanel('slides').querySelector('[name="selectedSlide"]');
   slidesList.onchange = changeSlide;
   var stepsList = getPanel('slides').querySelector('[name="selectedSteps"]');
   stepsList.onchange = changeSlideSteps;

   docInitFcts.push(catchSlides);
}


function catchSlides() {
   var list = getPanel('slides').querySelector('[name="selectedSlide"]');
   var listItem_None = list.firstElementChild;
   list.innerHTML = '';
   list.add(listItem_None);
   var stepsList = getPanel('slides').querySelector('[name="selectedSteps"]');
   stepsList.innerHTML = '';
   currentSlides = currentSteps = null;

   var slides = selectAllByXPath(svgDocument, './/*[@diagram:slideIndex]');
   for (var slideI = 0; slideI < slides.length; ++slideI) {
      var slideNumber = slides[slideI].getAttribute('diagram:slideIndex');
      var listItem = gui.createElement('option');
      listItem.text = slideNumber;
      listItem.value = slideNumber;
      list.add(listItem);
   }

   changeSlide();
}

function changeSlide() {
   var slidesList = getPanel('slides').querySelector('[name="selectedSlide"]');
   selectAllByXPath(svgDocument, './/*[@diagram:slideShowCurrent]').forEach(function(slideGroup) {
      slideGroup.removeAttribute('diagram:slideShowCurrent');
   });
   if (slidesList.value == '') {
      currentSlide = null;
   } else {
      currentSlide = selectByXPath(svgDocument, './/*[@diagram:slideIndex="' + slidesList.value + '"]');
      currentSlide.setAttributeNS(diagramNS, 'diagram:slideShowCurrent', '');
   }
   catchSlideSteps();
}

function catchSlideSteps() {
   var list = getPanel('slides').querySelector('[name="selectedSteps"]');
   list.innerHTML = '';

   if (currentSlide != null) {
      var stepNumber = 1;
      var slideSteps = selectAllByXPath(currentSlide, './/*[contains(@diagram:slideStepsIndexes, " ' + stepNumber + ' ")]');
      while (slideSteps.length > 0) {
         var listItem = gui.createElement('option');
         listItem.text = stepNumber;
         listItem.value = stepNumber;
         list.add(listItem);
         ++stepNumber;
         slideSteps = selectAllByXPath(currentSlide, './/*[contains(@diagram:slideStepsIndexes, " ' + stepNumber + ' ")]');
      }
   }

   currentSteps = null;
}

function changeSlideSteps() {
   var stepsList = getPanel('slides').querySelector('[name="selectedSteps"]');
   selectAllByXPath(currentSlide, './/*[@diagram:slideShowCurrent]').forEach(function(stepGroup) {
      stepGroup.removeAttribute('diagram:slideShowCurrent');
   });
   stepItems = stepsList.options;
   currentSteps = ' ';
   for (var itemI = 0; itemI < stepItems.length; ++itemI) {
      var stepItem = stepItems[itemI];
      if (stepItem.selected) {
         currentSteps += stepItem.value + ' ';
         var slideStepsGroups =
            selectAllByXPath(currentSlide, './/*[contains(@diagram:slideStepsIndexes, " ' + stepItem.value + ' ")]');
         slideStepsGroups.forEach(function(stepGroup) {
            stepGroup.setAttributeNS(diagramNS, 'diagram:slideShowCurrent', '');
         });
      }
   }
   if (currentSteps == ' ') {
      currentSteps = null;
   }
}


function compareStepsLists(list1, list2) {
   list1 = list1.trim().split(' ');
   list2 = list2.trim().split(' ');
   var stepI = 0;
   while (stepI < list1.length && stepI < list2.length && list1[stepI] == list2[stepI]) {
      ++stepI;
   }
   if (stepI == list1.length && stepI == list2.length) {
      return 0;
   } else if (stepI == list1.length) {
      return 1;
   } else if (stepI == list2.length) {
      return -1;
   } else if (list1[stepI] < list2[stepI]) {
      return -1;
   } else {
      return 1;
   }
}

function getStepsContainer(steps) {
   var stepsContainer = selectByXPath(currentSlide, './/*[@diagram:slideStepsIndexes="' + steps + '"]');
   if (stepsContainer == null) {
      stepsContainer = createSvgElement('g');
      stepsContainer.setAttributeNS(diagramNS, 'diagram:slideStepsIndexes', steps);
      stepsContainer.setAttributeNS(diagramNS, 'diagram:slideShowCurrent', '');
      var otherStepsContainers = selectAllByXPath(currentSlide, './/*[@diagram:slideStepsIndexes]');
      var containerI = 0;
      while (containerI < otherStepsContainers.length &&
         compareStepsLists(steps, otherStepsContainers[containerI].getAttribute('diagram:slideStepsIndexes')) > 0) {
         ++containerI;
      }
      if (containerI >= otherStepsContainers.length) {
         currentSlide.appendChild(stepsContainer);
      } else {
         otherStepsContainers[containerI].before(stepsContainer);
      }
   }
   return stepsContainer;
}


function addSlide() {
   var slidesList = getPanel('slides').querySelector('[name="selectedSlide"]');
   var slideNumber = slidesList.options.length;
   var slide = createSvgElement('g');
   slide.setAttributeNS(diagramNS, 'diagram:slideIndex', slideNumber);
   svgElement('content').appendChild(slide);
   var listItem = gui.createElement('option');
   listItem.text = slideNumber;
   listItem.value = slideNumber;
   slidesList.add(listItem);
   listItem.setAttribute('selected', '');
   changeSlide();
}

function putSlideUp() {
   if (currentSlide) {
      var curSlideNum = currentSlide.getAttribute('diagram:slideIndex');
      if (curSlideNum >= 2) {
         var slideToSwapWith = selectByXPath(svgDocument, './/*[@diagram:slideIndex="' + (curSlideNum - 1) + '"]');
         slideToSwapWith.setAttributeNS(diagramNS, 'diagram:slideIndex', curSlideNum);
         currentSlide.setAttributeNS(diagramNS, 'diagram:slideIndex', curSlideNum - 1);
         slideToSwapWith.before(currentSlide);
         var slidesList = getPanel('slides').querySelector('[name="selectedSlide"]');
         slidesList.selectedIndex = curSlideNum - 1;
      }
   }
}

function addSlideStep() {
   if (currentSlide != null) {
      var stepsList = getPanel('slides').querySelector('[name="selectedSteps"]');
      var stepNumber = stepsList.options.length + 1;
      var listItem = gui.createElement('option');
      listItem.text = stepNumber;
      listItem.value = stepNumber;
      stepsList.add(listItem);
   }
}


function putSelectionInContainer(container) {
   var selection = svgElement('content').querySelectorAll('.selected');
   for (var selI = 0; selI < selection.length; ++selI) {
      var elemParent = selection[selI].parentElement;
      putElementOnTop(selection[selI], container);
      if (elemParent.childElementCount == 0 && elemParent.getAttribute('diagram:slideStepsIndexes') != null) {
         elemParent.remove();
      }
   }
}

function putSelectionOutOfSlides() {
   putSelectionInContainer(svgElement('content'));
}

function putSelectionInCurrentSlideSteps() {
   var container = currentSlide;
   if (currentSteps) {
      container = getStepsContainer(currentSteps);
   }
   putSelectionInContainer(container);
}

function removeSlide() {
   if (currentSlide != null) {
      var curSlideNumber = parseInt(currentSlide.getAttribute('diagram:slideIndex'));
      currentSlide.remove();
      currentSlide = null;
      var allSlides = selectAllByXPath(svgDocument, './/*[@diagram:slideIndex]');
      for (var slideI = 0; slideI < allSlides.length; ++slideI) {
         var slideNum = parseInt(allSlides[slideI].getAttribute('diagram:slideIndex'));
         if (slideNum > curSlideNumber) {
            allSlides[slideI].setAttributeNS(diagramNS, 'diagram:slideIndex', slideNum - 1);
         }
      }
      var slidesList = getPanel('slides').querySelector('[name="selectedSlide"]');
      slidesList.selectedIndex = 0;
      slidesList.options[slidesList.options.length - 1].remove();
      changeSlide();
   }
}

function removeLastSlideStep() {
   var stepsList = getPanel('slides').querySelector('[name="selectedSteps"]');
   var stepNumber = stepsList.options.length;
   if (stepNumber > 0) {
      var slideStepsGroups = selectAllByXPath(currentSlide, './/*[contains(@diagram:slideStepsIndexes, " ' + stepNumber + ' ")]');
      if (slideStepsGroups.length == 0) {
         stepsList.options[stepsList.options.length - 1].remove();
      } else {
         alert("La dernière étape est encore utilisée. " +
            "Supprimez ou déplacez les éléments qui l’utilisent pour pouvoir supprimer l’étape.");
      }
   }
}
