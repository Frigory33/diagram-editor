window.addEventListener('load', function() {
   var panel = getPanel('main');
   panel.localId = 'main';
   (window.setupFileFeature || window.setupCodeFeature)();
   panel.appendChild(gui.createElement('br'));
   setupDocumentFeature();
   setupGridFeature();
   setupMarksFeature();
   panel.appendChild(gui.createElement('br'));
   setupSlidesFeature();
   setupStylesFeature();
   panel.appendChild(gui.createElement('br'));
   setupLayingFeature();
   setupShapesFeature();
   setupShadowFeature();

   addKeyboardFct('F4', function() {
      gui.element('panels').classList.toggle('hide');
      updateDocumentWrapperLayout();
   });

   rebaseDocument();
});


function addButton(label, fct, panel) {
   var button = gui.createElement('button');
   button.textContent = label;
   button.onclick = fct;
   (panel || getPanel('main')).appendChild(button);
}

function addPanel(id, fieldsDesc, buttonsDesc, mainButtonDesc) {
   var panel = gui.createElement('div');
   panel.id = id + 'Panel';
   panel.localId = id;
   panel.classList.add('panel');

   for (var fieldsetI = 0; fieldsetI < fieldsDesc.length; ++fieldsetI) {
      if (fieldsetI > 0) {
         panel.appendChild(gui.createElement('br'));
      }
      var fieldsetDesc = fieldsDesc[fieldsetI];
      if (fieldsetDesc.label) {
         var fieldsetTitle = document.createElement('span');
         fieldsetTitle.classList.add('fieldsetTitle');
         fieldsetTitle.textContent = "[" + fieldsetDesc.label + "]";
         panel.appendChild(fieldsetTitle);
      }
      var lastLabelAtRight = false;
      for (var fieldI = 0; fieldI < fieldsetDesc.fields.length; ++fieldI) {
         var fieldDesc = fieldsetDesc.fields[fieldI];
         if (fieldDesc.type == 'br') {
            panel.appendChild(gui.createElement('br'));
            if (fieldsetDesc.label) {
               var indent = fieldsetTitle.cloneNode(true);
               indent.style.visibility = 'hidden';
               panel.appendChild(indent);
            }
         } else {
            var label = gui.createElement('label');
            panel.append(" ");
            if (fieldDesc.type != 'checkbox') {
               label.append(fieldDesc.label + " : ");
               if (lastLabelAtRight) {
                  panel.append("| ");
                  lastLabelAtRight = false;
               }
            }
            var field;
            if (['select', 'multiselect'].indexOf(fieldDesc.type) >= 0) {
               field = gui.createElement('select');
               if (fieldDesc.options) {
                  for (var optionI = 0; optionI < fieldDesc.options.length; ++optionI) {
                     var optionDesc = fieldDesc.options[optionI];
                     var option = gui.createElement('option');
                     option.text = optionDesc.label;
                     option.value = optionDesc.id;
                     field.add(option);
                  }
               }
               if (fieldDesc.type == 'multiselect') {
                  label.appendChild(gui.createElement('br'));
                  field.setAttribute('multiple', '');
                  field.setAttribute('size', 6);
               }
            } else if (fieldDesc.type == 'textarea') {
               label.appendChild(gui.createElement('br'));
               field = gui.createElement('textarea');
            } else {
               field = gui.createElement('input');
               field.setAttribute('type', fieldDesc.type);
            }
            field.id = panel.id + '_' + fieldDesc.name;
            label.setAttribute('for', field.id);
            field.setAttribute('name', fieldDesc.name);
            if ('placeholder' in fieldDesc) {
               field.setAttribute('placeholder', fieldDesc.placeholder);
               field.setAttribute('title', "Par défaut : " + fieldDesc.placeholder);
            }
            if (fieldDesc.type == 'number') {
               field.setAttribute('step', 'any');
            }
            if ('defVal' in fieldDesc) {
               field[fieldDesc.type == 'checkbox' ? 'checked' : 'value'] = fieldDesc.defVal;
            }
            if ('styleSpec' in fieldDesc) {
               field.classList.add(fieldDesc.styleSpec);
            }
            label.appendChild(field);
            if (fieldDesc.type == 'color') {
               var fieldExt = gui.createElement('input');
               fieldExt.setAttribute('type', 'range');
               fieldExt.setAttribute('min', 0);
               fieldExt.setAttribute('max', 255);
               fieldExt.value = 0;
               if ('defVal' in fieldDesc) {
                  field.value = fieldDesc.defVal.substr(0, 7);
                  fieldExt.value = parseInt(fieldDesc.defVal.substr(7), 16);
               }
               field.onclick = function() {
                  if (this.nextElementSibling.value == 0) this.nextElementSibling.value = 255;
               };
               label.append(" ");
               label.appendChild(fieldExt);
            } else if (fieldDesc.type == 'checkbox') {
               label.append(" " + fieldDesc.label);
               lastLabelAtRight = true;
            }
            panel.appendChild(label);
         }
      }
   }

   panel.appendChild(gui.createElement('br'));
   for (var buttonI = 0; buttonI < buttonsDesc.length; ++buttonI) {
      var buttonDesc = buttonsDesc[buttonI];
      addButton(buttonDesc.label, buttonDesc.fct, panel);
   }

   gui.element('panels').appendChild(panel);

   if (mainButtonDesc) {
      var buttonFct;
      (function() {
         var panelName = id, initFct = mainButtonDesc.fct;
         buttonFct = function() {
            showPanel(id);
            if (initFct) {
               initFct();
            }
         };
      })();
      addButton(mainButtonDesc.label, buttonFct);
   }
}


function getPanelValues(panel) {
   if (!panel) panel = getPanel();
   var fields = panel.querySelectorAll('[name]');
   var data = {};
   for (var fieldI = 0; fieldI < fields.length; ++fieldI) {
      var field = fields[fieldI];
      var fieldType = field.getAttribute('type');
      var value = field.value;
      if (fieldType == 'number') {
         value = parseFloat(value);
         if (isNaN(value)) {
            value = null;
         }
      } else if (fieldType == 'checkbox') {
         value = field.checked ? true : null;
      } else if (fieldType == 'color') {
         var opacity = field.nextElementSibling.value;
         value = opacity == 0 ? null : value + to2HexDigits(opacity);
      }
      if (value != null && value !== '') {
         data[field.getAttribute('name')] = value;
      }
   }
   return data;
}

function setPanelValues(panel, data) {
   for (var fieldName in data) {
      var field = panel.querySelector('[name="' + fieldName + '"]');
      var fieldType = field.getAttribute('type');
      var value = data[fieldName];
      if (fieldType == 'checkbox') {
         field.checked = value;
      } else if (fieldType == 'color') {
         field.value = value ? value.substr(0, 7) : '#000000';
         field.nextElementSibling.value = value ? parseInt(value.substr(7), 16) : 0;
      } else {
         field.value = value;
      }
   }
}

function to2HexDigits(colorComp, colorCompI) {
   if (colorCompI == 3) colorComp *= 255;
   return (0x100 + parseInt(colorComp)).toString(16).substr(1);
}

function hasAllFields(data, fieldNames) {
   for (var fieldI = 0; fieldI < fieldNames.length; ++fieldI) {
      if (!(fieldNames[fieldI] in data)) {
         return false;
      }
   }
   return true;
}


function getPanel(id) {
   return id == undefined ? gui.querySelector('.panel.shown') : gui.element(id + 'Panel');
}

function showPanel(panelName) {
   getPanel().classList.remove('shown');
   getPanel(panelName || 'main').classList.add('shown');
   updateDocumentWrapperLayout();
}
function backFromPanel() {
   showPanel(); // Call without any arguments
}
