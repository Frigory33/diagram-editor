function setupMarksFeature() {
   addPanel('coordinateSystem',
      [
         {
            fields: [
               { label: "X1", type: 'number', name: 'x1', defVal: 0 },
               { label: "X2", type: 'number', name: 'x2', defVal: 10 },
               { label: "Y1", type: 'number', name: 'y1', placeholder: 'X1' },
               { label: "Y2", type: 'number', name: 'y2', placeholder: 'X2' },
            ],
         },
         {
            label: "Origine",
            fields: [
               { label: "X0", type: 'number', name: 'x0', defVal: 0 },
               { label: "Y0", type: 'number', name: 'y0', placeholder: 'X0' },
            ],
         },
         {
            label: "Marge",
            fields: [
               { label: "X (px)", type: 'number', name: 'mx', defVal: 5 },
               { label: "Y (px)", type: 'number', name: 'my', placeholder: 'X' },
            ],
         },
         {
            label: "Bloc",
            fields: [
               { label: "Largeur (px)", type: 'number', name: 'bw', placeholder: 'docL÷L' },
               { label: "Hauteur (px)", type: 'number', name: 'bh', placeholder: 'bL ou docH÷H' },
            ],
         },
      ],
      [
         { label: "Créer le repère", fct: createMarks },
         { label: "Annuler", fct: backFromPanel },
      ],
      { label: "Créer un repère" });
}


function createMarks() {
   setupGrid();

   svgElement('marks').innerHTML = svgElement('content').innerHTML = '';

   var xAxisGroup = createSvgElement('g', { id: 'xAxis' });
   var yOfXAxis = grid.docY0;
   var xAxis = createSvgElement('line', {
         'x1': grid.docX0 + (grid.x1 - grid.x0) * grid.bw - grid.bw / 4,
         'x2': grid.docX0 + (grid.x2 - grid.x0) * grid.bw + grid.bw / 4,
         'y1': yOfXAxis,
         'y2': yOfXAxis,
      });
   xAxisGroup.appendChild(xAxis);
   for (var x = grid.x1; x <= grid.x2; ++x) {
      if (x != grid.x0 || grid.y0 < grid.y1 || grid.y2 < grid.y0) {
         var xOnDoc = gridXToDocX(x);
         var stepMark = createSvgElement('line', {
               'x1': xOnDoc,
               'x2': xOnDoc,
               'y1': yOfXAxis - grid.bh / 8,
               'y2': yOfXAxis + grid.bh / 8,
            });
         xAxisGroup.appendChild(stepMark);
         var stepMarkText = createSvgElement('text', {
               'x': xOnDoc,
               'y': yOfXAxis + grid.bh / 4,
               'dy': '1ex',
            },
            x);
         xAxisGroup.appendChild(stepMarkText);
      }
   }
   svgElement('marks').appendChild(xAxisGroup);

   var yAxisGroup = createSvgElement('g', { id: 'yAxis' });
   var xOfYAxis = grid.docX0;
   var yAxis = createSvgElement('line', {
         'x1': xOfYAxis,
         'x2': xOfYAxis,
         'y1': grid.docY0 - (grid.y1 - grid.y0) * grid.bh + grid.bh / 4,
         'y2': grid.docY0 - (grid.y2 - grid.y0) * grid.bh - grid.bh / 4,
      });
   yAxisGroup.appendChild(yAxis);
   for (var y = grid.y1; y <= grid.y2; ++y) {
      if (y != grid.y0 || grid.x0 < grid.x1 || grid.x2 < grid.x0) {
         var yOnDoc = gridYToDocY(y);
         var stepMark = createSvgElement('line', {
               'x1': xOfYAxis - grid.bw / 8,
               'x2': xOfYAxis + grid.bw / 8,
               'y1': yOnDoc,
               'y2': yOnDoc,
            });
         yAxisGroup.appendChild(stepMark);
         var stepMarkText = createSvgElement('text', {
               'x': xOfYAxis - grid.bw / 4,
               'y': yOnDoc,
               'dy': '.5ex',
            },
            y);
         yAxisGroup.appendChild(stepMarkText);
      }
   }
   svgElement('marks').appendChild(yAxisGroup);

   backFromPanel();
}
