var mousePos = null;


function setupShadowFeature() {
   docInitFcts.push(initShadowOnDoc);
}


function initShadowOnDoc() {
   svgDocument.onmousemove = function(evt) {
      mousePos = { x: evt.clientX, y: evt.clientY };
      showShadow();
   };
   svgContainer.onmouseleave = function() {
      mousePos = null;
      showShadow();
   };
}


function showShadow() {
   svgElement('shadow').innerHTML = '';

   if (getPanel().localId in layingModes) {
      var data = getPanelValues();

      if (mousePos != null) {
         var point = { x: docXToGridX(mousePos.x), y: docYToGridY(mousePos.y) };

         if (getPanel().localId != 'text') {
            var shadow = createSvgElement('path', {
                  'd': 'M ' + gridXToDocX(point.x) + ',' + gridYToDocY(point.y) + ' m -5,-5 l 10,10 m -10,0 l 10,-10',
                  'style': 'stroke-width: 1px;',
               });
            svgElement('shadow').appendChild(shadow);
         }

         var fields = getPointFields();
         if (fields) {
            ['x', 'y'].forEach(function(coordName) {
               if (fields[coordName] && (fields.mode != 'diff' || pointsList.length > 0)) {
                  data[fields[coordName].getAttribute('name')] =
                     fields.mode == 'diff' ? point[coordName] - pointsList[pointsList.length - 1][coordName] : point[coordName];
               }
            });
         }
      }
      layElementSomewhere(svgElement('shadow'), data);
   }
}
