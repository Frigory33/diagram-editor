var svgContainer, svgDocument, svgRoot, diagramNS;

var docInitFcts = [];


function setupDocumentFeature() {
   addPanel('metadata',
      [
         {
            fields: [
               { label: "Titre", type: 'text', name: 'title' },
            ],
         },
         {
            fields: [
               { label: "Description", type: 'textarea', name: 'desc' },
            ],
         }
      ],
      [
         { label: "Valider", fct: changeMetadata },
         { label: "Annuler", fct: backFromPanel },
      ],
      { label: "Changer les métadonnées", fct: initMetadataPanel });

   addPanel('size',
      [{
         fields: [
            { label: "Largeur", type: 'number', name: 'width' },
            { label: "Hauteur", type: 'number', name: 'height' },
         ],
      }],
      [
         { label: "Appliquer", fct: changeSize },
         { label: "Annuler", fct: backFromPanel },
      ],
      { label: "Changer la taille", fct: initSizePanel });

   svgContainer = gui.querySelector('object');
   svgContainer.onload = initDocument;
}


function initDocument() {
   svgDocument = svgContainer.contentDocument;
   svgRoot = svgDocument.rootElement;
   diagramNS = svgDocument.rootElement.getAttribute('xmlns:diagram');

   svgDocument.onkeydown = svgDocument.onmouseup = gui.onkeydown;
   svgDocument.onkeyup = gui.onkeyup;
   svgDocument.oncontextmenu = function() {
      return false;
   };

   for (var fctI = 0; fctI < docInitFcts.length; ++fctI) {
      docInitFcts[fctI]();
   }

   updateDocumentWrapperLayout();
}

function rebaseDocument(fileName) {
   svgContainer.data = fileName == undefined ? 'base.svg' : 'diagrams/' + fileName;
}


function svgElement(id) {
   return svgDocument.getElementById(id);
}

function createSvgElement(tagName, attributes, content) {
   var element = svgDocument.createElementNS('http://www.w3.org/2000/svg', tagName);
   for (var attrName in attributes) {
      element.setAttribute(attrName, attributes[attrName]);
   }
   if (content != undefined) {
      element.textContent = content;
   }
   return element;
}


function initSizePanel() {
   var panel = getPanel('size');
   panel.querySelector('[name="width"]').value = parseInt(svgRoot.getAttribute('width'));
   panel.querySelector('[name="height"]').value = parseInt(svgRoot.getAttribute('height'));
}

function changeSize() {
   var data = getPanelValues();
   svgRoot.setAttribute('width', data.width);
   svgRoot.setAttribute('height', data.height);
   backFromPanel();
}


function initMetadataPanel() {
   ['title', 'desc'].forEach(function(metadataName) {
      getPanel('metadata').querySelector('[name="' + metadataName + '"]').value =
         svgDocument.querySelector(metadataName).textContent;
   });
}

function changeMetadata() {
   var data = getPanelValues();
   ['title', 'desc'].forEach(function(metadataName) {
      svgDocument.querySelector(metadataName).textContent = data[metadataName];
   });
   backFromPanel();
}
