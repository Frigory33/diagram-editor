var defaultFileName = 'diagram.svg';
var documentFileName = defaultFileName;


function setupFileFeature() {
   addButton("Réinitialiser", clearDocument);

   addPanel('openFile',
      [{
         fields: [
            { label: "Nom", type: 'text', name: 'fileName', defVal: documentFileName }
         ],
      }],
      [
         { label: "Charger", fct: changeDocumentSourceFile },
         { label: "Annuler", fct: backFromPanel },
      ],
      { label: "Appréhender un fichier", fct: initFilePanel });

   addButton("Enregistrer", saveDocumentFile);

   addPanel('saveFile',
      [{
         fields: [
            { label: "Nom", type: 'text', name: 'fileName', defVal: documentFileName }
         ],
      }],
      [
         { label: "Enregistrer", fct: saveDocumentFileAs },
         { label: "Enregistrer comme cliché", fct: saveDocumentFileAsShot },
         { label: "Annuler", fct: backFromPanel },
      ],
      { label: "Enregistrer sous", fct: initFilePanel });
}


function initFilePanel() {
   getPanel().querySelector('[name="fileName"]').value = documentFileName;
}


function isolateFileName(path) {
   var separatorIndex = Math.max(path.lastIndexOf('/'), path.lastIndexOf('\\'));
   if (separatorIndex < 0) {
      return ['.', path];
   }
   return [path.substring(0, separatorIndex), path.substring(separatorIndex + 1)];
}


function clearDocument() {
   documentFileName = defaultFileName;
   rebaseDocument();
}

function changeDocumentSourceFile() {
   var data = getPanelValues(getPanel('openFile'));
   documentFileName = isolateFileName(data.fileName)[1];
   rebaseDocument(documentFileName);
   backFromPanel();
}

function saveDocumentFile() {
   var documentSource = new XMLSerializer().serializeToString(svgDocument);

   var xhr = new XMLHttpRequest();
   xhr.open('POST', 'save.php', true);
   xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
   xhr.send('fileName=' + encodeURIComponent(documentFileName) + '&content=' + encodeURIComponent(documentSource));
}

function saveDocumentFileAs() {
   var data = getPanelValues(getPanel('saveFile'));
   documentFileName = isolateFileName(data.fileName)[1];
   saveDocumentFile();
   backFromPanel();
}

function saveDocumentFileAsShot() {
   var oldDocumentFileName = documentFileName;
   saveDocumentFileAs();
   documentFileName = oldDocumentFileName;
}
