var gui = document;
gui.element = gui.getElementById;

var keyboardFcts = {}, currentKeyModifiers = [];


window.addEventListener('load', function() {
   gui.onkeydown = function(evt) {
      var keyDesc = '';
      var mouseButtonNames = ['left', 'middle', 'right'];
      if (evt.ctrlKey || evt.key == 'Control') keyDesc += 'Ctrl-';
      if (evt.altKey || evt.key == 'Alt') keyDesc += 'Alt-';
      if (evt.shiftKey || evt.key == 'Shift') keyDesc += 'Shift-';
      if (evt.key) keyDesc += evt.key.length == 1 ? evt.key.toUpperCase() : evt.key;
      if (evt.button in mouseButtonNames) keyDesc += 'Mouse ' + mouseButtonNames[evt.button];
      currentKeyModifiers = keyDesc.split('-').slice(0, -1);
      if ('' in keyboardFcts) {
         keyboardFcts[''].forEach(function(fct) {
            fct();
         });
      }
      if (keyDesc in keyboardFcts) {
         keyboardFcts[keyDesc].forEach(function(fct) {
            fct(evt);
         });
         return false;
      }
   };
   gui.onkeyup = function(evt) {
      var index = currentKeyModifiers.indexOf(evt.key == 'Control' ? 'Ctrl' : evt.key);
      if (index >= 0) currentKeyModifiers.splice(index, 1);
      if ('' in keyboardFcts) {
         keyboardFcts[''].forEach(function(fct) {
            fct();
         });
      }
   };
});


function selectByXPath(elem, xPath) {
   var doc = elem.ownerDocument || elem;
   return doc.evaluate(xPath, elem, doc.createNSResolver(doc.documentElement), XPathResult.ANY_UNORDERED_NODE_TYPE).
      singleNodeValue;
}
function selectAllByXPath(elem, xPath) {
   var doc = elem.ownerDocument || elem;
   var elemsIter = doc.evaluate(xPath, elem, doc.createNSResolver(doc.documentElement), XPathResult.UNORDERED_NODE_ITERATOR_TYPE);
   var elems = [], elem;
   while (elem = elemsIter.iterateNext()) {
      elems.push(elem);
   }
   return elems;
}


function addKeyboardFct(trigger, fct) {
   if (!(trigger in keyboardFcts)) {
      keyboardFcts[trigger] = [];
   }
   keyboardFcts[trigger].push(fct);
}


function updateDocumentWrapperLayout() {
   var documentWrapper = gui.element('documentWrapper');
   documentWrapper.style.top = gui.element('panels').offsetHeight + 'px';
}
